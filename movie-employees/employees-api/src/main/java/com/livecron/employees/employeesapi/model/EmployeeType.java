package com.livecron.employees.employeesapi.model;

/**
 * @author Hans Hamel
 */
public enum EmployeeType {
    STUDENT_TYPE,
    TEACHER_TYPE;
}
