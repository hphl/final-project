package com.livecron.employees.employeesapi.output;

/**
 * @author Hans Hamel
 */
public interface IEmployee {
    Long getUserId();

    String getFirstName();

    String getLastName();
}
