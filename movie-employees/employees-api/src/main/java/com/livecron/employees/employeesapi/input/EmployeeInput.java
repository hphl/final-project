package com.livecron.employees.employeesapi.input;

import com.livecron.employees.employeesapi.output.IEmployee;

/**
 * @author Hans Hamel
 */
public class EmployeeInput implements IEmployee {//it is correct that the employeeInput implements the inteface IEMployee???
    private Long userId;
    private String firstName;
    private String lastName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
