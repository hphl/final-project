package com.livecron.employees.employees.service.service;

import com.livecron.employees.employees.service.model.domain.Employee;
import com.livecron.employees.employees.service.model.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * @author Hans Hamel
 */
abstract class AbstractEmployeeService {

    private Employee employee;

    @Autowired
    private EmployeeRepository repository;

    public void execute() {
        employee = findEmployee(repository);
    }

    protected abstract Employee findEmployee(EmployeeRepository repository);

    public Employee getEmployee() {
        return employee;
    }

}
