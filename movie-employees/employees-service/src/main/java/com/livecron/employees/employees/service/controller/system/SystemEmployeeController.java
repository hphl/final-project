package com.livecron.employees.employees.service.controller.system;

import com.livecron.employees.employees.service.service.EmployeeCreateService;
import com.livecron.employees.employeesapi.input.EmployeeInput;
import com.livecron.employees.employeesapi.output.IEmployee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Hans Hamel
 */
@Api(
        tags = "employee-controller",
        description = "Operations over employees"
)
@RequestMapping(value = "/system/employees")
@RestController
@RequestScope
public class SystemEmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @ApiOperation(
            value = "create vacation"
    )
    @RequestMapping(
            method = RequestMethod.POST
    )
    public IEmployee createEmployee(@RequestBody EmployeeInput employee) {
        employeeCreateService.setInput(employee);
        employeeCreateService.execute();

        return employeeCreateService.getEmployee();
    }

}
