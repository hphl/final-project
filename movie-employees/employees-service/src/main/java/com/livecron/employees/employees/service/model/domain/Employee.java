package com.livecron.employees.employees.service.model.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.livecron.employees.employeesapi.model.EmployeeType;
import com.livecron.employees.employeesapi.output.IEmployee;

import java.util.Date;

/**
 * @author Hans Hamel
 */
@Entity
@Table(name = "Employee_Table")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Employee implements IEmployee {
    @Id
    @Column(name = "employeeid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "userId", nullable = false)
    private Long userId;
    @Column(name = "firstname", nullable = false)
    private String firstName;
    @Column(name = "lastname", nullable = false)
    private String lastName;
    private Boolean active;
    @Enumerated(EnumType.STRING)
    private EmployeeType type;
    private Date CreatedDate;

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public EmployeeType getType() {
        return type;
    }

    public void setType(EmployeeType type) {
        this.type = type;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }
}
