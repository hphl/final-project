package com.livecron.employees.employees.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Hans Hamel
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket publicApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("employees-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.livecron.employees.employees.service.controller.api"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    @Bean
    public Docket publicSystem() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("employees-system")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.livecron.employees.employees.service.controller.system"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Employees Service API")
                .description("Employees management")
                .contact(new Contact("Hans", "", "hp_hamel@hotmail.com"))
                .version("0.0.1")
                .license("Apache 1.0")
                .licenseUrl("https://docs.spring.io/spring-data/jpa/docs/current/reference/html/")
                .build();
    }
}
