package com.livecron.employees.employees.service.service;

import com.livecron.employees.employees.service.model.domain.Employee;
import com.livecron.employees.employees.service.model.repository.EmployeeRepository;
import com.livecron.employees.employeesapi.input.EmployeeInput;
import com.livecron.employees.employeesapi.model.EmployeeType;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Hans Hamel
 */
@Service
public class EmployeeCreateService extends AbstractEmployeeService {
    private EmployeeInput input;

    @Override
    protected Employee findEmployee(EmployeeRepository repository) {
        return repository.save(composeEmployeeInstance());
    }

    private Employee composeEmployeeInstance() {
        Employee instance = new Employee();
        instance.setUserId(input.getUserId());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setActive(Boolean.FALSE);
        instance.setCreatedDate(new Date());
        instance.setType(EmployeeType.STUDENT_TYPE);

        return instance;
    }

    public void setInput(EmployeeInput input) {
        this.input = input;
    }
}
