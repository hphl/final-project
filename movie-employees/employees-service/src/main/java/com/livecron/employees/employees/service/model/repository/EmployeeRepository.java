package com.livecron.employees.employees.service.model.repository;

import com.livecron.employees.employees.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Hans Hamel
 */

public interface EmployeeRepository extends JpaRepository<Employee, String> {

}
