package com.livecron.binaries.service.Service;

import com.livecron.binaries.service.model.domain.Binary;
import org.springframework.stereotype.Service;

/**
 * @author Hans Hamel
 */
@Service
public class BinaryCreateService extends AbstractBinaryService {

    @Override
    protected Binary loadBinaryInstance() {
        return new Binary();
    }
}
