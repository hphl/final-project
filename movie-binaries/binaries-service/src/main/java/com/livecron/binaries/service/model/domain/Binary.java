package com.livecron.binaries.service.model.domain;

import com.livecron.binaries.api.output.IBinary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Collection;

/**
 * @author Hans Hamel
 */

@Document(collection = "binary")
public class Binary implements IBinary {

    @Id
    private String id;

    @Field("type")
    private String mimeType;

    @Field("name")
    private String name;

    private Long size;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
