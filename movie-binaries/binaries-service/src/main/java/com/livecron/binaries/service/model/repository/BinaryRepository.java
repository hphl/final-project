package com.livecron.binaries.service.model.repository;

import com.livecron.binaries.service.model.domain.Binary;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Hans Hamel
 */
public interface BinaryRepository extends MongoRepository<Binary, String> {
}
