package com.livecron.binaries.api.output;

/**
 * @author Hans Hamel
 */
public interface IBinary {
    String getId();

    String getMimeType();

    String getName();

    Long getSize();
}
