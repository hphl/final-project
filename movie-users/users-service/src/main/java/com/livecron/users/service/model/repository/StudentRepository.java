package com.livecron.users.service.model.repository;

import com.livecron.users.service.model.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
