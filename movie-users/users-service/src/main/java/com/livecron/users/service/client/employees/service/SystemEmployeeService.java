package com.livecron.users.service.client.employees.service;

import com.livecron.employees.employeesapi.input.EmployeeInput;
import com.livecron.users.service.client.employees.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SystemEmployeeService {

    @Autowired
    private SystemEmployeeClient client;

    public Employee createEmployee(Employee employee) {
        Employee instance = employee;
        return client.createEmployee(instance);
    }
}
