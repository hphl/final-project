package com.livecron.users.service.client.employees.model;

import com.livecron.employees.employeesapi.output.IEmployee;

/**
 * @author Hans Hamel
 */
public class Employee implements IEmployee {
    private Long employeeId;
    private String firstName;
    private String lastName;

    @Override
    public Long getUserId() {
        return employeeId;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setUserId(Long studentId) {
        this.employeeId = studentId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
