package com.livecron.users.service.controller;

import com.livecron.users.api.input.StudentCreateInput;
import com.livecron.users.service.model.domain.Student;
import com.livecron.users.service.service.StudentCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;


@RequestMapping(value = "/students")
@RequestScope
@RestController
public class StudentController {

    @Autowired
    private StudentCreateService studentCreateService;

    @RequestMapping(method = RequestMethod.POST)
    public Student createStudent(@RequestBody StudentCreateInput input) {
        studentCreateService.setInput(input);
        studentCreateService.execute();

        return studentCreateService.getStudent();
    }
}
