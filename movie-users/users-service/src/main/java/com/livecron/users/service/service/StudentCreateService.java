package com.livecron.users.service.service;

import com.livecron.employees.employeesapi.input.EmployeeInput;
import com.livecron.users.api.input.StudentCreateInput;
import com.livecron.users.api.model.AccountState;
import com.livecron.users.service.client.employees.model.Employee;
import com.livecron.users.service.client.employees.service.SystemEmployeeService;
import com.livecron.users.service.config.UserProperties;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.domain.Student;
import com.livecron.users.service.model.repository.AccountRepository;
import com.livecron.users.service.model.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StudentCreateService {

    private StudentCreateInput input;

    private Student student;

    @Autowired
    private SystemEmployeeService systemEmployeeService;

    @Autowired
    private UserProperties userProperties;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private StudentRepository studentRepository;

    public void execute() {
        Account account = composeAccountInstance();
        account = accountRepository.save(account);

        Student instance = composeStudentInstance(account);
        student = studentRepository.save(instance);
        Employee employee = composeEmployeeInstance(student);
        systemEmployeeService.createEmployee(employee);
    }

    private Employee composeEmployeeInstance(Student student) {
        Employee instance = new Employee();
        instance.setFirstName(student.getFirstName());
        instance.setLastName(student.getLastName());
        instance.setUserId(student.getId());

        return instance;
    }

    private Account composeAccountInstance() {
        AccountState state = readState();
        Account account = new Account();
        account.setEmail(input.getEmail());
        account.setState(state);

        return account;
    }

    private AccountState readState() {
        return input.getAge() >= userProperties.getAge() ? AccountState.ACTIVATED : AccountState.DEACTIVATED;
    }

    private Student composeStudentInstance(Account account) {
        Student instance = new Student();
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setPassword(input.getPassword());
        instance.setNote(input.getNote());
        instance.setActive(Boolean.TRUE);
        instance.setCreatedDate(new Date());
        instance.setAccount(account);

        return instance;
    }

    public void setInput(StudentCreateInput input) {
        this.input = input;
    }

    public Student getStudent() {
        return student;
    }
}
