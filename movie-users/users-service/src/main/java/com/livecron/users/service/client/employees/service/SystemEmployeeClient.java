package com.livecron.users.service.client.employees.service;

import com.livecron.employees.employeesapi.input.EmployeeInput;
import com.livecron.users.service.client.employees.model.Employee;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "employees-service")
interface SystemEmployeeClient {

    @RequestMapping(
            value = "/system/employees",
            method = RequestMethod.POST
    )
    Employee createEmployee(@RequestBody Employee employee);
}
